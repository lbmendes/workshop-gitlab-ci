# FROM php:apache
# COPY ./index.html ./bemvindo.php /var/www/html/
FROM alpine
RUN apk --no-cache add apache2 php8-apache2 tzdata
COPY ./index.html ./bemvindo.php /var/www/localhost/htdocs/
EXPOSE 80
ENV TZ="America/Bahia"
ENTRYPOINT ["httpd", "-D", "FOREGROUND"]
