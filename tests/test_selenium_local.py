"""Módulo de testes da aplicação usando Selenium."""
from selenium.webdriver import Firefox
from time import sleep
from webdriver_manager.firefox import GeckoDriverManager


def test_msg_bemvindo_has_the_right_content():
    """Preenche form e checa se conteúdo da msg bemvindo está como esperado."""
    person = "Maria"
    driver = Firefox(executable_path=GeckoDriverManager().install())
    driver.get('http://localhost/')
    sleep(3)
    driver.find_element_by_id('name').send_keys(person)
    sleep(3)
    driver.find_element_by_css_selector('[value="Enviar"]').click()
    sleep(3)
    body_text = driver.find_element_by_tag_name('body').text
    driver.save_screenshot('selenium.png')
    driver.quit()
    assert body_text == f'Bem vindo(a) {person} !'
